#include "core/main.h"
#include "core/core.hpp"

#include <queue>
#include "hermes-plus.hpp"

/* CAN RELATED FLAGS AND DATA */
uint8_t canErrFlag = 0;
uint32_t canErrCode = 0;
uint8_t rxCount = 0, txCount = 0;
uint8_t canOnlineFlag = 1;
uint32_t canDisconnectTime = 0;

/** TALOS SLEEP RELATED DATA */
micro_second sleepStartTime = 0;

CAN_HandleTypeDef hcan1;

bool READY = false;

// Buffers
std::queue<std::string> outMsgs; // From self to PC
std::queue<std::string> inMsgs; // From PC to self

std::queue<Hermes::CANBuffer*> outFrames; // From self to CAN
std::queue<Hermes::CANBuffer*> inFrames;  // From CAN to self

std::vector<uint8_t> inBuffer;

bool setupCan (CAN_HandleTypeDef *hcan1);

void rxCan (
    CAN_HandleTypeDef *hcan, uint32_t fifo, 
    std::queue<Hermes::CANBuffer*>& frames, 
    std::queue<std::string>& msgs
);

void txCan (
    CAN_HandleTypeDef *hcan,
    Hermes::CANBuffer *frame,
    std::queue<std::string>& msgs
);

void processSignal (std::string& signal);

void setup() {

    HAL_Init();
    SystemClock_Config();

    delay(500);

    pinMode(LED_BUILTIN, OUTPUT);

    digitalWrite(LED_BUILTIN, HIGH);

    Serial.begin(1000000);  

    if (!setupCan(&hcan1)) {
        Serial.println("@err:can_setup_err");
        return;
    }

    if (HermesCAN_setupIt(&hcan1) == HERMES_CAN_ERROR) {
        Serial.println("@err:can_it_setup_err");
        return;
    }

    Serial.println("@ok:ready");

    READY = true;
    digitalWrite(LED_BUILTIN, LOW);

}

void SystemClock_Config();

void loop() {

    if (!READY) {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(250);
        digitalWrite(LED_BUILTIN, LOW);
        delay(250);
        return;
    }

    /**
     * CAN ERROR HANDLER
     */
    canErrCode = HAL_CAN_ERROR_NONE;
    if (canErrFlag) {
        canErrCode = HAL_CAN_GetError(&hcan1);
        HAL_CAN_ResetError(&hcan1);
        canErrFlag = 0;
        Serial.printf("@err:can_err_flag:%d\r\n", canErrCode);
        return;
    }

    /**
     * Process USART TX
     */
    for (txCount = outMsgs.size(); txCount; txCount--) {
        digitalWrite(LED_BUILTIN, HIGH);
        Serial.println(outMsgs.front().c_str());
        outMsgs.pop();
        digitalWrite(LED_BUILTIN, LOW);
    }

    /**
     * Process USART RX 
     */
    for (rxCount = inMsgs.size(); rxCount; rxCount--) {
        std::string &input = inMsgs.front();
        if (input[0] == '@') {
            processSignal(input);
            inMsgs.pop();
            continue;
        }
        auto *tempBuffer = new Hermes::CANBuffer;
        if(!Hermes::parseBuffer(input, tempBuffer)) {
            Serial.println(("@err:invalid_buffer:" + input).c_str());
        } else {
            outFrames.push(tempBuffer);
        }
        inMsgs.pop();
    }

    /**
     * PROCESS CAN TX
     */
    for (txCount = outFrames.size(); txCount; txCount--) {
        digitalWrite(LED_BUILTIN, HIGH);
        auto frame = outFrames.front();
        txCan(&hcan1, frame, outMsgs);
        outFrames.pop();
        delete frame;
        digitalWrite(LED_BUILTIN, LOW);
    }
    
    /**
     * PROCESS CAN RX
     */
    for (rxCount = inFrames.size(); rxCount; rxCount--) {
        auto frame = inFrames.front();
        outMsgs.push(Hermes::serializeBuffer(frame));
        inFrames.pop();
        delete frame;
    }

}

void processSignal (std::string& signal) {

    std::string cmd = "";
    std::string arg = "";

    int i;
    for (i = 1; i < signal.size(); i++) {
        if (signal[i] == ':') {
            break;
        }
        cmd += signal[i];
    }

    for (i++; i < signal.size(); i++) {
        if (signal[i] == ':') {
            break;
        }
        arg += signal[i];
    }

    if (cmd == "echo") {
        outMsgs.push("@print:" + arg);
    } 
    
    else if (cmd == "led") {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(1000);
        digitalWrite(LED_BUILTIN, LOW);
    }

}

void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan) {
    
}

void HAL_CAN_TxMailbox1CompleteCallback(CAN_HandleTypeDef *hcan) {
    
}

void HAL_CAN_TxMailbox2CompleteCallback(CAN_HandleTypeDef *hcan) {

}

void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan) { canErrFlag = 1; }

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan) {
    rxCan(hcan, CAN_RX_FIFO0, inFrames, outMsgs);
}

void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan) {
    rxCan(hcan, CAN_RX_FIFO1, inFrames, outMsgs);
}

void Hermes_onReservedMessage(Hermes_t *hermes, HermesBuffer *buffer) {

}

bool setupCan (CAN_HandleTypeDef *hcan1) {

    if (!IS_CAN_MODE(HERMES_CAN_MODE)) {
        return HERMES_CAN_ERROR;
    }

    hcan1->Instance = CAN1;
    hcan1->Init.Mode =
        HERMES_CAN_MODE;  // FORCE RX TO HIGH VERY IMPORTANT IF TIMEOUT ERROR
                          // (0x60000) IN LOOPBACK MODE

    // 80MHz
    #ifdef CORE_F446RE
    hcan1->Init.Prescaler = 8;
    #else 
    hcan1->Init.Prescaler = 16;
    #endif
    hcan1->Init.TimeSeg1 = CAN_BS1_2TQ;
    hcan1->Init.TimeSeg2 = CAN_BS2_2TQ;
    hcan1->Init.SyncJumpWidth = CAN_SJW_1TQ;

    hcan1->Init.AutoRetransmission = ENABLE;
    hcan1->Init.TimeTriggeredMode = DISABLE;
    hcan1->Init.AutoWakeUp = DISABLE;
    hcan1->Init.AutoBusOff = ENABLE;
    hcan1->Init.TransmitFifoPriority = ENABLE;
    hcan1->Init.ReceiveFifoLocked = DISABLE;

    if (HAL_CAN_Init(hcan1) != HAL_OK) {
        return false;
    }

    CAN_FilterTypeDef filter;
     /* Device filter setup */
    filter.FilterActivation = CAN_FILTER_ENABLE;
    filter.FilterBank = 0;
    filter.FilterFIFOAssignment = CAN_RX_FIFO0;
    filter.FilterMode = CAN_FILTERMODE_IDMASK;
    filter.FilterScale = CAN_FILTERSCALE_32BIT;

    filter.FilterMaskIdHigh = 0x0;
    filter.FilterMaskIdLow = 0x0;

    filter.FilterIdHigh = 0x0;
    filter.FilterIdLow = 0x0;

    if (HAL_CAN_ConfigFilter(hcan1, &filter) != HAL_OK) {
        return HERMES_CAN_ERROR;
    }

    uint32_t err = HAL_CAN_Start(hcan1);
    if (err != HAL_OK) {
        return false;
    }

    return true;

}

void rxCan (
    CAN_HandleTypeDef *hcan1, uint32_t fifo, 
    std::queue<Hermes::CANBuffer*>& frames, 
    std::queue<std::string>& msgs
) {

    CAN_RxHeaderTypeDef header;
    uint8_t payload[8];
    uint8_t sender;
    uint8_t receiver;
    uint16_t command;
    bool isResponse;
    HermesReturnCode code;

    if (HAL_CAN_GetRxMessage(hcan1, fifo, &header, payload) != HAL_OK) {
        msgs.push("@err:can_rx_fifo:" + std::to_string(fifo));
        return;
    }

    Hermes_parseCANFrameID(header.ExtId, &receiver, &command, &sender,
                           &isResponse);

    auto buf = new Hermes::CANBuffer;
    buf->command = command;
    buf->sender = sender;
    buf->destination = receiver;
    buf->isResponse = isResponse;
    buf->len = header.DLC;
    std::copy(payload, payload + buf->len, buf->data);

    frames.push(buf);

}

void txCan (
    CAN_HandleTypeDef *hcan,
    Hermes::CANBuffer *frame,
    std::queue<std::string>& msgs
) {

    CAN_TxHeaderTypeDef header;
    header.ExtId = Hermes_serializeCANFrameID(
        frame->destination, frame->command, 
        frame->sender, frame->isResponse
    );
    header.IDE = CAN_ID_EXT;
    header.RTR = CAN_RTR_DATA;
    header.DLC = frame->len;

    uint32_t txbox;
    if (HAL_CAN_AddTxMessage(hcan, &header, frame->data, &txbox) != HAL_OK) {
        msgs.push("@err:can_tx:" + std::to_string(txbox));
    }

}

std::string buffer = "";
void serialEvent () {

    if (!Serial.available()) return;

    char c = Serial.read();

    if (c == '\r') {
        inMsgs.push(buffer);
        buffer = "";
        return;
    }

    buffer += c;

}

#ifdef CORE_F446RE
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 80;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}
#else
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }

  /** Enable MSI Auto calibration
  */
  HAL_RCCEx_EnableMSIPLLMode();
}
#endif