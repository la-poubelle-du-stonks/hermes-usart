# Core STM
This guide briefly describes how to setup a STM32 Core peripheral.

## Introduction
It might be intriguing at first, but it is really just like using the Arduino Library with overlays. In fact, you can use Arduino functions and libraries as well as STM32 HAL functions.

## Setup
Follow these steps to properly setup a new peripheral.
1. Download [platform.io](https://platformio.org/platformio-ide). Our architecture is based on it and it works like a charm.
3. Fork this repository and name it after the peripheral you are building.
4. Clone the newly created repository onto your device. `git clone --recurse-submodules <your-repo-link.git>`
2. Once cloned, you must setup the STM32 platform on the platform.io tab in VSCode.
5. Write your program in the `user.cpp`. **DO NOT WRITE FILES THAT ARE IN THE CORE FOLDER.**
6. Build your program using `CTRL+ALT+B`.
7. Upload your program using `CTRL+ALT+U`. Make sure your target is plugged into your computer.
8. To setup the debug mode, go on the platformio panel in VSCode then Click `Quick Access > Debug > Start Debugging`. It will open and configure VSCode Debug panel. This has to be done the first time your using the debugger.
9. When debugging, access the VSCode debug panel and select `PIO Debug`. You can now access memory, registers, variables, call stack and breakpoints.


## Guidelines

### Hardware setup
The CAN Controller requires 2 dedicated pins in order to communicate over the network. `D10` is **RX** and `D2` is **TX**. You must not use them.

### Software setup
Instead of writing your program in Arduino's predefined functions, you must write them in the corresponding functions, inside the provided `user.cpp` file.

|Arduino|Core|
|---|----|
|`void init()`| `void Talos_initialisation()`|
|`void loop()`| `void Talos_loop()`|

When a error occurs inside the Core program, `void Talos_loop()` will be paused and `void Talos_onError()` will be executed instead. It allows the developer to define some emergency functions to be executed like stopping wheels, or even forcing a restart using `Core_hardReset()` or `Core_softReset()`.You can force the error mode using `coreSelfCheck->forceError();`.

If the error cleans itself (ex: the CAN link is up again), `void Talos_loop()` will start again and `void Talos_onError()` will stop. Be careful, when exiting the error state, `void Talos_initialisation()` will be **executed once again**. You might not want your pin to be assigned again.

**NB:** `void Talos_onError()` is a looping function.

### Send a message over CAN


### Receive a message over CAN


### Advanced usage
In case you have functions that need to be running no matter Core's state(ex: wheels automation), they can be run in `void Core_priorityLoop()`.

## Selfcheck mechanism
Core *hates* infinite loops.
By design, such loops prevent the systemm from processing CAN related messages, therefore we had to implement a self check (SC) mechanism.
This mechanism is periodically checking if the system is stucked into a loop.
To do so, it is using the `TIM15` timer peripheral and the `csetjmp` library.
We also use this mechanism to check if the CAN network is up when it's too quiet.

### Usage
The SC is enabled by default. It can be disabled **AT YOUR OWN RISK** using `coreSelfCheck->disable();`.
If an infinite loop (**wait time longer than 1000 ms**) or a CAN error is detected, Talos goes into error mode and `void Talos_onError()` is started.
Wait time is defined by `CORE_SELFCHECK_WAIT_TIME_MS` and thus can be tweaked.

## Prototyping with Core
Developers can prototype their peripherals without being annoyed by the selfcheck mechanism or by being forced to connect to the CAN network. it allows developers to independently develop and test their peripherals.

You must perfom the following operations :
- In `plaformio.ini`, set `HERMES_CAN_MODE` to `CAN_MODE_SILENT_LOOPBACK`
- In `plaformio.ini`, set `CORE_LOG_SERIAL` if not set.
- You must add `coreSelfCheck->disable();` at the beginning of `Talos_initialisation()`.

Don't forget to set `HERMES_CAN_MODE` back to `CAN_MODE_NORMAL` when your peripheral to reach the network.