#ifndef UNIT_H
#define UNIT_H

typedef float mm;
typedef float mm_per_s;
typedef float mm_per_s_per_s;

typedef float rad;
typedef float rad_per_s;
typedef float rad_per_s_per_s;

typedef float rot;
typedef float rot_per_s;
typedef float rpm;

typedef int pin;

typedef unsigned long milli_second;
typedef unsigned long micro_second;
typedef unsigned long hertz;

#endif
